package boardgame;

import java.util.Random;

/**
 * A generic (fair) die for use in a game. Can have any number of values.
 * 
 * @author Ethan Ruffing
 * @since 2014-10-04
 *
 * @param <T>
 *        The type of values each face of the die will hold
 */
public class Die<T> implements IDie<T> {
	/** The values for each side of the die */
	private T[] values;

	/** A random generator to use when rolling the die */
	private Random rn;

	/** The index of the face that is up */
	private int faceUp;

	/**
	 * Constructs a new die with the specified values on each side. Creates a
	 * new random generator for use in rolling the die.
	 *
	 * @param values
	 *        The values to populate the die with
	 */
	public Die(T[] values) {
		this(values, new Random());
	}

	/**
	 * Constructs a new die with the specified values on each side and using the
	 * specified random number generator.
	 * 
	 * @param values
	 *        The values to populate the die with
	 * @param rn
	 *        The random number generator to use
	 */
	public Die(T[] values, Random rn) {
		this.setValues(values);
		this.rn = rn;
		this.faceUp = 0;
	}

	/**
	 * "Rolls" the die, selecting a side to display at random.
	 * 
	 * @return A randomly-selected value from the die
	 */
	public T rollDie() {
		int i = this.rn.nextInt(this.values.length);
		this.faceUp = i;
		return this.getFace(this.faceUp);
	}

	/**
	 * Service method to access the value on the face that is currently up of
	 * the die.
	 * 
	 * @return The value on the face that is facing up (the value that was
	 *         rolled)
	 */
	public T getFaceUp() {
		return this.getFace(this.faceUp);
	}

	/**
	 * Gets the index of the face that is currently up.
	 *
	 * @return The index of the face that is currently up.
	 */
	public int getFaceUpIndex() {
		return faceUp;
	}

	/**
	 * Service method to access a specific value on the die.
	 *
	 * @param i
	 *        The index of the value to access
	 * @return The value at the specified index
	 */
	public T getFace(int i) {
		return this.values[i];
	}

	/**
	 * Service method to get the values in the die.
	 *
	 * @return The array of values that the die contains
	 */
	public T[] getValues() {
		return values;
	}

	/**
	 * Service method to set the values for the die.
	 *
	 * @param values
	 *        The values to place on the die
	 */
	public void setValues(T[] values) {
		this.values = values;
	}
}
