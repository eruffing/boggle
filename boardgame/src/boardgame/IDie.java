package boardgame;

/**
 *  An interface for a generic (fair) die for use in a game. Can have any number
 *  of values.
 *
 * @author Ethan Ruffing
 * @since 2016-01-29
 *
 * @param <T>
 *        The type of values each face of the die will hold
 */
public interface IDie<T> {
	/**
	 * "Rolls" the die, selecting a side to display at random.
	 *
	 * @return A randomly-selected value from the die
	 */
	T rollDie();

	/**
	 * Service method to access the value on the face that is currently up of
	 * the die.
	 *
	 * @return The value on the face that is facing up (the value that was
	 *         rolled)
	 */
	T getFaceUp();

	/**
	 * Gets the index of the face that is currently up.
	 *
	 * @return The index of the face that is currently up.
	 */
	int getFaceUpIndex();

	/**
	 * Service method to access a specific value on the die.
	 *
	 * @param i
	 *        The index of the value to access
	 * @return The value at the specified index
	 */
	T getFace(int i);

	/**
	 * Service method to get the values in the die.
	 *
	 * @return The array of values that the die contains
	 */
	T[] getValues();

	/**
	 * Service method to set the values for the die.
	 *
	 * @param values
	 *        The values to place on the die
	 */
	void setValues(T[] values);
}
