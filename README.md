Boggle
======
Copyright 2014, 2016 by Ethan Ruffing.

This is a Boggle game written in pure Java. For use on the command line.

The game is currently only capable of generating the board. Actual gameplay must
be human-regulated until such features are added in a future release.

Program Options
===============
The following is the output of the help option, which gives a brief overview
of all available commands.

    usage: boggle [-h] [-s] [-t <arg>] [-v]
     -h,--help         show the help output
     -s,--solve        show solutions
     -t,--type <arg>   specify the type of game
     -v,--version      show version information

Game Type
---------
The available game types are `BOGGLE` (for a 4x4 game) and `BIGBOGGLE` (for a
5x5 game).
