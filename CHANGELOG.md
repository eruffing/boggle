Changelog
=========

## 1.1.0 ##
* Some improvement to the solver's efficiency by adding parallelization.

## 1.0.0 ##
* Small and large games working.
* Some command line options available.
* Built-in solver working (albeit inefficiently).

## 0.1.0 ##
* Initial release. Small game is working.
