package boggle.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.concurrent.Callable;
import java.util.stream.IntStream;

/**
 * A tool for solving a Boggle game.
 *
 * @author Ethan Ruffing
 * @since 2016-01-28
 */
public class BoggleSolver {
	private String[][] board;
	HashSet<String> dictionary;
	HashSet<String> solutions;

	/**
	 * Constructs a new BoggleSolver for the given board.
	 *
	 * @param board The board to solve.
	 *
	 * @throws IOException Thrown if an error occurs while reading the
	 *                     dictionary.
	 */
	public BoggleSolver(String[][] board) throws IOException {
		dictionary = new HashSet<String>();

		InputStream in = getClass().getResourceAsStream("/dict.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		while (br.ready()) {
			dictionary.add(br.readLine().toUpperCase());
		}

		this.board = board;
		solutions = new HashSet<>();
	}

	/**
	 * Determine the possible solutions for the board.
	 *
	 * @return All possible solutions.
	 */
	public String[] solve() {
		solutions = new HashSet<String>();

		boolean[][] used = new boolean[board.length][board[0].length];

		IntStream.range(0, board.length)
				.parallel()
				.forEach(i0 ->
						IntStream.range(0, board[0].length)
								.parallel()
								.forEach(i1 ->
										solve(i0, i1, "", new boolean[board.length][board[0].length])
								)
				);

		return solutions.toArray(new String[solutions.size()]);
	}

	private Callable<Void> toCallable(final Runnable runnable) {
		return new Callable<Void>() {
			@Override
			public Void call() {
				runnable.run();
				return null;
			}
		};
	}

	/**
	 * Recursively finds all solutions starting at a given location.
	 *
	 * @param row      The row of the die to start at.
	 * @param col      The column of the die to start at.
	 * @param solution The solution that exists so far.
	 * @param used     The spots that have been used so far.
	 */
	private void solve(int row, int col, String solution, boolean[][] used) {
		solution += board[row][col];
		used[row][col] = true;
		if (dictionary.contains(solution) && !solutions.contains(solution)) {
			solutions.add(solution);
		}
		String solt = solution;
		IntStream.range(row > 0 ? row - 1 : 0, row + 1 < board.length ? row + 2 : board.length)
				.parallel()
				.forEach(i0 ->
						IntStream.range(col > 0 ? col - 1 : 0, col + 1 < board[0].length ? col + 2 : board[0].length)
								.parallel()
								.forEach(i1 -> {
											if (!used[i0][i1] && !(i0 == row && i1 == col)) {
												boolean[][] tused = new boolean[used.length][used[0].length];
												for (int i3 = 0; i3 < used.length; i3++)
													System.arraycopy(used[i3], 0, tused[i3], 0, used[i3].length);

												solve(i0, i1, solt, used);
												for (int i3 = 0; i3 < tused.length; i3++)
													System.arraycopy(tused[i3], 0, used[i3], 0, tused[i3].length);
											}
										}
								)
				);
	}
}
