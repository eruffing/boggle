package boggle;

import org.json.JSONException;

import java.io.IOException;

/**
 * A frontend system to handle the gameplay of a Boggle game.
 *
 * @author Ethan Ruffing
 * @since 2014-10-07
 */
public class BoggleGame {
	/**
	 * The dice to use for the game
	 */
	private BoggleDice dice;

	/**
	 * Creates a new standard Boggle game.
	 *
	 * @throws JSONException if the given game type is not found in the file.
	 * @throws IOException if the GameTypes.json file cannot be read.
	 */
	public BoggleGame() throws IOException, JSONException {
		this("BOGGLE");
	}

	/**
	 * Creates a new Boggle game.
	 *
	 * @param gameType The type of Boggle game to create.
	 *
	 * @throws JSONException if the given game type is not found in the file.
	 * @throws IOException if the GameTypes.json file cannot be read.
	 *
	 */
	public BoggleGame(String gameType) throws IOException, JSONException {
		dice = new BoggleDice(gameType);
	}

	/**
	 * Gets a representation of the generated board.
	 *
	 * @return A multi-dimensional array representing the current board.
	 */
	public String[][] getBoard() {
		return dice.getFaceUps();
	}

	/**
	 * Displays the current state of the board.
	 */
	public void displayBoard() {
		this.dice.rollDice();
		String[][] s = this.dice.getFaceUps();

		for (int i0 = 0; i0 < s.length; i0++) {
			for (int i1 = 0; i1 < s[i0].length; i1++) {
				String si = s[i0][i1];
				System.out.print(si);
				if (si.length() < 2) {
					for (int i2 = 0; i2 < si.length(); i2++) {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}
}
