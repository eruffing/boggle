package boggle;

import java.util.Random;

/**
 * A die that is designed to be populated with letters or strings.
 *
 * @author Ethan Ruffing
 * @since 2014-10-04
 */
public class StringDie extends boardgame.Die<String> {
	/**
	 * @param values
	 */
	public StringDie(String[] values) {
		super(values);
	}

	/**
	 * @param values
	 * @param rn
	 */
	public StringDie(String[] values, Random rn) {
		super(values, rn);
	}

}
