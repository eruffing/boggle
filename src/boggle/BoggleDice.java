package boggle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A set of dice used for a Boggle game.
 *
 * @author Ethan Ruffing
 * @since 2014-10-04
 */
public class BoggleDice {

	/**
	 * The set of dice to use for the game.
	 */
	private StringDie[][] dice;

	/**
	 * Constructs a set of dice for a basic boggle game by calling the
	 * constructor {@link BoggleDice(String) this(String)} with
	 * parameter <code>"BOGGLE"</code>.
	 *
	 * @throws JSONException if the given game type is not found in the file.
	 * @throws IOException if the GameTypes.json file cannot be read.
	 */
	public BoggleDice() throws IOException, JSONException {
		this("BOGGLE");
	}

	/**
	 * Constructs a set of dice for the specified type of Boggle game.
	 *
	 * @param type The type of boggle game to create
	 *
	 * @throws JSONException if the given game type is not found in the file.
	 * @throws IOException if the GameTypes.json file cannot be read.
	 */
	public BoggleDice(String type) throws IOException, JSONException {
		populate(type);
	}

	/**
	 * Populates the dice faces for a game of normal Boggle.
	 *
	 * @param gameType The type of game to be played
	 *
	 * @throws IOException if the GameTypes.json file cannot be read.
	 * @throws JSONException if the given gameType is not found in the
	 *     definitions.
	 */
	private void populate(String gameType) throws IOException, JSONException {
		InputStream in = getClass().getResourceAsStream("/GameTypes.json");
		List<String> lines =
				new BufferedReader(new InputStreamReader(in))
						.lines()
						.collect(Collectors.toList());
		String json = "";

		for (int i = 0; i < lines.size(); i++) {
			json += lines.get(i);
		}

		JSONObject obj = new JSONObject(json);

		JSONArray diceSet = obj.getJSONArray(gameType);

		int boardLength = (int)Math.sqrt(diceSet.length());
		dice = new StringDie[boardLength][boardLength];

		for (int i0 = 0; i0 < dice.length; i0++) {
			for (int i1 = 0; i1 < dice[i0].length; i1++) {
				String[] die = new String[6];
				for (int i2 = 0; i2 < 6; i2++) {
					die[i2] = diceSet.getJSONArray(i0 + i1).getString(i2);
				}
				dice[i0][i1] = new StringDie(die);
			}
		}
	}

	/**
	 * Rolls all the dice in this set of dice.
	 *
	 * @return An array of all rolled values
	 */
	public String[][] rollDice() {
		String[][] faces = new String[this.dice.length][this.dice[0].length];

		for (int i0 = 0; i0 < dice.length; i0++) {
			for (int i1 = 0; i1 < dice[i0].length; i1++) {
				faces[i0][i1] = this.dice[i0][i1].rollDie();
			}
		}

		return faces;
	}

	/**
	 * Service method to get the face up values on all dice in this set.
	 *
	 * @return An array of the face up (rolled) values on all dice
	 */
	public String[][] getFaceUps() {
		String[][] faces = new String[this.dice.length][this.dice[0].length];

		for (int i0 = 0; i0 < this.dice.length; i0++) {
			for (int i1 = 0; i1 < dice[0].length; i1++) {
				faces[i0][i1] = this.dice[i0][i1].getFaceUp();
			}
		}

		return faces;
	}

}
