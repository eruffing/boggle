package boggle.cli;

import boggle.BoggleGame;
import boggle.solver.BoggleSolver;
import org.apache.commons.cli.*;
import org.json.JSONException;

import java.io.IOException;

public class Boggle {

	private static String version = "1.1.0";
	private static String copyright = "Copyright 2014, 2016 by Ethan Ruffing.";

	public static void main(String[] args) {
		// Define the options
		Options options = new Options();
		options.addOption("h", "help", false, "show the help output");
		options.addOption("v", "version", false, "show version information");
		options.addOption("t", "type", true, "specify the type of game");
		options.addOption("s", "solve", false, "show solutions");

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		BoggleGame game = null;

		// Parse the options
		try {
			cmd = parser.parse(options, args);

		} catch (ParseException e) {
			System.out.println("Error when parsing arguments.");
			showHelp(options);
			System.exit(1);
		}

		if (cmd.hasOption("help") || cmd.hasOption('h')) {
			showHelp(options);
			System.exit(0);
		} else if (cmd.hasOption("version") || cmd.hasOption("v")) {
			showVersion();
			System.exit(0);
		} else if (cmd.hasOption("type") || cmd.hasOption('t')) {
			try {
				game = new BoggleGame(cmd.getOptionValue("type"));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				System.out.println("Specified game type not found.");
			}
		} else {
			try {
				game = new BoggleGame("BOGGLE");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				System.out.println("Failed to create standard Boggle game.");
				e.printStackTrace();
			}
		}

		// Run the game
		game.displayBoard();

		if (cmd.hasOption("s") || cmd.hasOption("solve")) {
			try {
				BoggleSolver bs = new BoggleSolver(game.getBoard());
				System.out.println();
				System.out.println("Solutions:");
				String[] sols = bs.solve();
				System.out.println("(" + sols.length + " found)");
				for (String s : sols) {
					System.out.println(s);
				}
			} catch (IOException e) {
				System.out.println("Failed to read dictionary.");
				e.printStackTrace();
			}
		}
	}

	public static void showHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("boggle", options, true);
	}

	public static void showVersion() {
		System.out.println("Boggle, Version " + version);
		System.out.println(copyright);
	}

}
